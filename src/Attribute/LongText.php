<?php

namespace Stylemix\Listing\Attribute;

class LongText extends Base
{
	/**
	 * Adds attribute mappings for elastic search
	 *
	 * @param \Illuminate\Support\Collection $mapping Mapping to modify
	 */
	public function elasticMapping($mapping)
	{
		$mapping[$this->name] = ['type' => 'text'];
	}

	/**
	 * Adds attribute casts
	 *
	 * @param \Illuminate\Support\Collection $casts
	 */
	public function applyCasts($casts)
	{
		$casts->put($this->name, 'string');
	}

	/**
	 * @inheritdoc
	 */
	public function formField()
	{
		return \Stylemix\Base\Fields\Textarea::make($this->fillableName)
			->placeholder($this->placeholder)
			->required($this->required)
			->multiple($this->multiple)
			->label($this->label);
	}

}
