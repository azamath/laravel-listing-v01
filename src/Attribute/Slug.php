<?php

namespace Stylemix\Listing\Attribute;

use Illuminate\Support\Str;

/**
 * @property string $generateFrom
 */
class Slug extends Keyword
{

	public function __construct(string $name = null) {
		$name = $name ?? 'slug';
		$this->generateFrom = 'title';
		parent::__construct($name);
	}

	public function applyDefaultValue($attributes)
	{
		if ($this->generateFrom instanceof \Closure) {
			return call_user_func($this->generateFrom, $attributes);
		}
		else {
			return Str::slug($attributes->get($this->generateFrom) ?? Str::random(8));
		}
	}

}
