<?php

namespace Stylemix\Listing\Elastic;

use Elasticquent\ElasticquentPaginator;

class Paginator extends ElasticquentPaginator
{

	public function __construct($items, $hits, $total, $perPage, $currentPage = null, array $options = [])
	{
		parent::__construct($items, $hits, $total, $perPage, $currentPage, $options);

		$maxResultsSize = config('listing.max_result_window', 10000);
		$this->lastPage = min(ceil($maxResultsSize / $perPage), $this->lastPage);
	}
}
