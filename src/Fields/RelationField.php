<?php

namespace Stylemix\Listing\Fields;

use Stylemix\Base\Fields\Base;

class RelationField extends Base
{

	public $component = 'relation-field';

}
