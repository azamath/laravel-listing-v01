<?php

namespace Stylemix\Listing;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class EntityBuilder extends Builder
{

	/**
	 * @var \Stylemix\Listing\Entity
	 */
	protected $model;

	public function insert(array $values)
	{
		return parent::insert($values);
	}

	public function insertGetId(array $values, $sequence = null)
	{
		list ($values, $attributes) = $this->splitAttributeValues($values);

		if ($id = parent::insertGetId($values, $sequence)) {
			foreach ($attributes as $key => $value) {
				if (is_null($value)) {
					continue;
				}

				$this->model->dataAttributes->push(
					$this->model->dataAttributes()->forceCreate([
						'entity_id' => $id,
						'name' => $key,
						'value' => $value,
					])
				);
			}
		}

		return $id;
	}

	public function update(array $values)
	{
		list ($values, $attributes) = $this->splitAttributeValues($values);

		$updated = count($values) > 0 ? parent::update($values) : 1;

		foreach ($attributes as $name => $value) {
			/** @var \Stylemix\Listing\EntityData $attribute */
			$attribute = $this->model->dataAttributes->where('name', $name)->first();

			if (!$attribute) {
				if (is_null($value)) {
					continue;
				}
				$this->model->dataAttributes->push(
					$attribute = $this->model->dataAttributes()->newModelInstance([
						'name' => $name,
					])
				);
			}

			$attribute->value = $value;
			$attribute->entity()->associate($this->model);

			if (is_null($attribute->value)) {
				$attribute->delete();
			} else {
				$attribute->save();
			}
		}

		return $updated;
	}

	public function get($columns = ['*'])
	{
		$collection = parent::get($columns);

		return $collection->each(function ($entity) {
			$this->hydrateDataAttributes($entity);
		});
	}

	protected function hydrateDataAttributes(Entity $entity)
	{
		$values = array_merge($entity->getAttributes(), $entity->dataAttributes->pluck('value', 'name')->all());
		$entity->setRawAttributes($values, true);
	}

	protected function splitAttributeValues(array $values)
	{
		$attributes = Arr::except($values, $this->model->dbFields);
		$values = Arr::only($values, $this->model->dbFields);

		return [$values, $attributes];
	}

}
